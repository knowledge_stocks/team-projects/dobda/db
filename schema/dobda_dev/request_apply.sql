/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

CREATE TABLE IF NOT EXISTS `request_apply` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `amount` int unsigned NOT NULL,
  `description` varchar(200) COLLATE utf8mb4_general_ci NOT NULL,
  `reg_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `state` tinyint unsigned NOT NULL,
  `helper_id` int unsigned NOT NULL,
  `request_id` bigint unsigned NOT NULL,
  `mod_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UKss7ftjb1nx3y5u77yqdjcs9ui` (`request_id`,`helper_id`),
  KEY `FKk2wb7mgtrvqvdap2il5w2tr9a` (`helper_id`),
  CONSTRAINT `FKffo9dxfk8i1heop8j17197i9w` FOREIGN KEY (`request_id`) REFERENCES `request` (`id`),
  CONSTRAINT `FKk2wb7mgtrvqvdap2il5w2tr9a` FOREIGN KEY (`helper_id`) REFERENCES `member_helper` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;

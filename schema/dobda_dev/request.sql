/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

CREATE TABLE IF NOT EXISTS `request` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `address1` varchar(128) COLLATE utf8mb4_general_ci NOT NULL,
  `address2` varchar(128) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `axis_x` double NOT NULL,
  `axis_y` double NOT NULL,
  `category` int unsigned NOT NULL,
  `deadline` datetime DEFAULT NULL,
  `description` mediumblob NOT NULL,
  `dong_code` varchar(10) COLLATE utf8mb4_general_ci NOT NULL,
  `is_can_suggest_payment` tinyint(1) NOT NULL,
  `mod_time` datetime DEFAULT NULL,
  `payment` int unsigned NOT NULL,
  `reg_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `report_count` int unsigned NOT NULL DEFAULT '0',
  `sido_code` varchar(2) COLLATE utf8mb4_general_ci NOT NULL,
  `sigungu_code` varchar(5) COLLATE utf8mb4_general_ci NOT NULL,
  `state` tinyint unsigned NOT NULL,
  `title` varchar(50) COLLATE utf8mb4_general_ci NOT NULL,
  `view_count` int unsigned NOT NULL DEFAULT '0',
  `writer_id` int unsigned DEFAULT NULL,
  `ignore_report` tinyint(1) NOT NULL DEFAULT '0',
  `sort_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `FK942e5ojsmuqv3bkixilm1ax9s` (`writer_id`),
  CONSTRAINT `FK942e5ojsmuqv3bkixilm1ax9s` FOREIGN KEY (`writer_id`) REFERENCES `member` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

CREATE TABLE IF NOT EXISTS `review` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `content` varchar(200) COLLATE utf8mb4_general_ci NOT NULL,
  `evaluation` tinyint unsigned NOT NULL,
  `reg_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `order_id` bigint unsigned NOT NULL,
  `target_id` int unsigned NOT NULL,
  `writer_id` int unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK17jxl2h30nlkfieiysp5rl444` (`order_id`,`writer_id`),
  KEY `FKkn9bj8f5no3yf2mgagiicsex3` (`target_id`),
  KEY `FKrfth9722gng6enmc2rhbqnbh2` (`writer_id`),
  CONSTRAINT `FK80acgchiskxpcqegik62mf1jg` FOREIGN KEY (`order_id`) REFERENCES `order` (`id`),
  CONSTRAINT `FKkn9bj8f5no3yf2mgagiicsex3` FOREIGN KEY (`target_id`) REFERENCES `member` (`id`) ON DELETE CASCADE,
  CONSTRAINT `FKrfth9722gng6enmc2rhbqnbh2` FOREIGN KEY (`writer_id`) REFERENCES `member` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
